# Tennessee Eastman Plant (Larsson et al.) 
This anonymous repository contains the MATLAB/Simulink project of the Tennessee Eastman Plant (TEP) and the results of experiments executed on it.

All the credit for this implementation(s) goes to N. Lawrence Ricker (http://depts.washington.edu/control/LARRY/TE/download.html).

# Simulation
The TEP has been a subject of study for many years and there are different implementations by multiple authors. This repository contains different implementations of the TEP. Our experiments were executed exclusively on one of those implementations. Specifically, the implementation proposed by Larsson et. al (http://www.nt.ntnu.no/users/skoge/publications/2001/te/submitted_version/te.pdf). The file of this particular model is [MultiLoop_Skoge_mode1.mdl](/simulation/TEmatlabLarsson/MultiLoop_Skoge_mode1.mdl).

# Experiments
Our experiments focus on manipulations of sensor readings. The integrity attacks on sensors are executed according to the following approach: 

1. starting with an execution of the system without any attacks and recording the minimum (min) and maximum (max) values observed per sensor;
2. computing the difference diff=max−min for each sensor; and 
3. executing the attacks on sensors, one at a time, compromising their integrity with 4 fixed values: min, max, min−diff , and max+diff. We make sure that the values min − diff and max + diff are within the expected physical limits of each sensor (e.g., level sensors always ≥ 0).

We execute simulated attacks against all sensors and take note of the corresponding shutdown times (if any). We execute 52 attacks on each sensor: we consider 13 different plant conditions and execute 4 integrity attacks per sensor for each of them. This specific  control strategy cannot handle disturbance #6 (loss of one of the input raw products). Therefore, we removed it from our set of experiments. Taking into account the 16 sensors used in this control strategy, we execute a total of 832 individual simulations that add up
to 19,943.616 simulated hours (∼2.27 years).

Under the [experiments](/experiments/) directory we document the experments' results and screenshots of each individual experiment.
